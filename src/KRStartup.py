import os
import time
import psutil
import signal
import rospy
import tkinter as tk
from threading import Thread
from idlelib.tooltip import Hovertip
from KRUtils import *
from typing import Callable, List, Dict
from Inspectable import Inspectable
from rosnode import get_node_names

class KRStartup_Ctrl(Inspectable):
  """
  Controller for the KRStartup_View.
  Inspectable to be callable from the RobCog frontend.
  """

  def __init__(self) -> None:
    self.started_processes: dict[str, psutil.Process] = {}
    self.config: dict = load_config()
    super().__init__()

  def is_roscore_running(self) -> bool:
    """
    Checks if roscore is running.

    :return: True if the core is running. False otherwise.
    :rtype: bool
    """
    for process in psutil.process_iter():
      combined = " ".join(process.cmdline())
      if "rosmaster" in combined and "--core" in combined:
        return True
    return False

  def restart_roscore(self) -> bool:
    """
    Restart ROSCore.

    :return: True unconditionally
    :rtype: bool
    """
    self._stop_process("roscore")
    self.start_roscore()
    return True

  def start_roscore(self) -> bool:
    """
    Starts ROSCore.

    :return: True if the core did sucessfully started. False otherwise
    :rtype: bool
    """
    if "roscore" not in self.started_processes.keys():
      self.started_processes["roscore"] = run_cmd_in_gnome_term("roscore", title="ROSCORE")
    return busy_wait(lambda : self.is_roscore_running(),title="roscore")

  def _is_node_running(self, node_name : str) -> bool:
    """
    Returns the state of the given node.

    :return: True if the Node is running. False otherwise.
    :rtype: bool
    """
    return node_name in " ".join(get_node_names())
  
  def list_nodes(self) -> List:
    """
    Returns a list of all running ROS nodes.

    :return: List of ROS nodes.
    :rtype: List
    """
    return get_node_names()
    
  def _start_node(self, node_name: str, package: str, executable: str) -> bool:
    """
    Starts the given ROS node.  

    :param node_name: Name of the node to be started.
    :type node_name: str
    :param package: Package in which the node is located.
    :type package: str
    :param executable: Executable of the node to be initiated.
    :type executable: str
    :return: True if the node is already running or was started successfully. False otherwise.
    :rtype: bool
    """
    if self._is_node_running(node_name):
      return True
    else:
      self.started_processes[node_name] = run_cmd_in_gnome_term("roslaunch {} {}".format(package,executable), title=node_name)
      return busy_wait(lambda : self._is_node_running(node_name),title=node_name)  
          
  def start_rest_server(self) -> Dict:
    rest_server_ip = get_lan_ip()
    rest_server_port = self.config.get("rest_server_port",8000)
    """
    Starts the REST API Server. Necessary to start end stop NEEM recording.

    :return: ip and port of REST server.
    :rtype: dict
    """
    self._start_node("rosbridge_websocket","rosbridge_server","rosbridge_websocket.launch")
    if "rest_server" not in self.started_processes.keys():
      def get_RESTClient_impl():
        for root, _, files in os.walk('.'):
          if "RESTClient.py" in files:
            return os.path.join(root,"RESTClient.py")
        print("RESTClient.py not found in subdirectories!")
        raise NotImplementedError
      self.started_processes["rest_server"] = run_cmd_in_gnome_term("python3 {} --host {} --port {}".format(get_RESTClient_impl(),rest_server_ip,rest_server_port),title="REST Server")
    return {"host":rest_server_ip, "port":rest_server_port}
  
  def restart_rest_server(self) -> Dict:
    """
    Restarts the REST Server. If not started yet, it starts the server.

    :return: The IP address and port on which the server is listening.
    :rtype: dict
    """
    self._stop_process("rest_server")
    return self.start_rest_server()
  
  def start_rviz(self) -> bool:
    """
    Starts RVIZ.

    :return: True whe Rviz is started. False when no package/executable for RViz launch is defined in config.
    :rtype: bool
    """
    package = self.config.get("rviz_launch_package")
    executable = self.config.get("rviz_launch_executable")
    if package and executable:
      self._start_node("rviz",package,executable)
      rospy.wait_for_service("/rviz/load_config")
      return True
    else:
      print("No RViz package and/or executable defined in config file. Will not start RViz.")
      return False
  
  def restart_rviz(self) -> bool:
    """
    Restarts RVIZ. If not started yet, starts RVIZ.

    :return: True unconditionally.
    :rtype: bool
    """
    self._stop_process("rviz")
    return self.start_rviz()
  
  def start_knowrob(self) -> bool:
    """
    Starts KnowRob.

    :return: True unconditionally.
    :rtype: bool
    """
    self._start_node("rosprolog","knowrob","knowrob.launch")
    rospy.wait_for_service("/rosprolog/query")
    return True
  
  def restart_knowrob(self) -> bool:
    """
    Restarts KnowRob. If not yet started, starts KnowRob.

    :return: True unconditionally
    :rtype: bool
    """
    self._stop_process("rosprolog")
    return self.start_knowrob()

  def list_processes(self) -> List:
    """
    Lists all processes that have been started by using this tool.

    :return: List of all started processes.
    :rtype: list
    """
    res = []
    for k,v in self.started_processes.items():
      res.append("{} -> {}".format(k,v))
    return res

  def _stop_process(self,name: str) -> bool:
    """
    Stops the process with the given name.

    :param name: Name of the process to be stopped (as entered in the map of started processes).
    :type name: str
    :return: True unconditionally
    :rtype: bool
    """
    if name in self.started_processes.keys():
      if self.started_processes[name]:
        process = self.started_processes[name]
        print("Terminating process [{}] (PID: {})".format(process.cmdline(),process.pid))
        process.send_signal(signal.SIGINT)
        if not busy_wait(lambda: not process.is_running(), max_wait=5, title="Process {}".format(process.cmdline())):
          print("Process {} was not interruptable. Killing it now.".format(process.cmdline()))
          process.kill()
      del self.started_processes[name]
    else:
      print("No process with name: {} is running".format(name))
    return True
  
  def stop_all_processes(self) -> bool:
    """
    Stops all processes that have been started by this tool.

    :return: True unconditionally
    :rtype: bool
    """
    for p in self.started_processes.keys():
      self._stop_process(p)
    return True

class KRHStartup_View:
  """
  If you want to use the KRStartup_Ctrl without the frontend within RobCog,
  you can use this class to get a simple GUI to interface with the the functions of KRStartup_Ctrl 
  (and some additional functions for local MongoDB/Prolog Interfaces).
  """

  def _add_button(self, text : str, command: Callable) -> tk.Button:
    """
    Configuring and adding a button to the main GUI of the startup interface.
    """
    btn = tk.Button(self.root, text=text, width=BTN_WIDTH, command=command)
    btn.pack(pady=PADY,padx=PADX)
    return btn

  def _on_closing(self) -> None:
    """
    Called when the X-button is pressed in the top-right corner.
    """
    self.close_window = True

  def __init__(self) -> None:
    self.POLL_FREQ = 2
    self.HOOVER_DELAY = 400
    self.close_window = False
    self.ctrl = KRStartup_Ctrl()
    self.root = tk.Tk()
    self.root.title("KnowRob Startup Helper")
    self.root.protocol("WM_DELETE_WINDOW", self._on_closing)

    self.button_knowrob = self._add_button("Start KnowRob", lambda : self.ctrl.start_knowrob())
    self.button_prolog = self._add_button("Start ROSProlog", lambda : run_cmd_in_gnome_term("rosrun rosprolog rosprolog_commandline.py"))
    self.button_robot3t = self._add_button("Start Robo3T", lambda: run_cmd_in_gnome_term("robo3t-snap </dev/null &>/dev/null &"))
    self.button_reasoning = self._add_button("Start Reasoning Interface", lambda: run_cmd_in_gnome_term("python3 {}".format("KRReasoner.py")))
    
    Hovertip(self.button_knowrob,'Requires the KnowRob project to be sourced.',hover_delay=self.HOOVER_DELAY)
    Hovertip(self.button_reasoning,'Requires ROS Master Running.',hover_delay=self.HOOVER_DELAY)

    def set_gui_state_based_on_ros_master_state(self) -> None:
      """
      Makes sure that the ROS Master is running.
      Gracefully closes the program when the Thread that is doing the polling is interupted.
      """
      while not self.close_window:
        try:
          self.button_reasoning["state"]= "normal" if self.ctrl.is_ros_master_running() else "disabled"
        except RuntimeError:
          print("Main Window closed. Bye.")
          return
        time.sleep(self.POLL_FREQ)
      self.root.destroy()
      return

    Thread(target=set_gui_state_based_on_ros_master_state, args=(self,)).start()
    self.root.mainloop()

if __name__ == "__main__":
  KRHStartup_View()