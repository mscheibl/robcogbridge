from subprocess import Popen
import os
import json
import bson
import psutil
from psutil import Process
from pymongo import MongoClient
from time import sleep
from subprocess import Popen, check_output
from typing import Callable
from pathlib import Path
from netifaces import interfaces, ifaddresses, AF_INET

PADY = 4
PADX = 2
BTN_WIDTH = 50

def load_config() -> dict:
  """
  Loads the config.json file and parses it.
  """
  if os.path.isfile("./config.json"):
    with open("./config.json", "r") as f:
      return json.load(f)
  else:
    print("No config.json found")
    print(os.listdir())
    return {}
  
config = load_config()

def run_cmd_in_gnome_term(cmd: str, title : str = "") -> Process:
  """
  Start the given command in a new gnome command line window.

  :param cmd: Command to run.
  :type cmd: str
  :return: Process object.
  :rtype: Popen
  """
  if not title:
     title = cmd
  Popen("gnome-terminal --tab --title \"{}\" -- {}".format(title,cmd), shell=True)
  sleep(1)
  processes_with_cmd_in_command = list(filter(lambda ps : cmd in " ".join(ps.cmdline()),psutil.process_iter()))
  try:
    print("Started Process [{}] (PID:{})".format(cmd,processes_with_cmd_in_command[0].pid))
  except IndexError as e:
    print("It seems like no Process with the given command [{}] is running.".format(cmd))
    print(e)
    return None
  return processes_with_cmd_in_command[0]

def run_cmd_in_tmux_term(cmd: str, title : str = "") -> Process:
  """
  Start the given command in a new tmux command line window.

  :param cmd: Command to run.
  :type cmd: str
  :return: Process object.
  :rtype: Popen
  """
  if not title:
     title = cmd
  Popen("tmux split-window \"cmd\"".format(cmd), shell=True)
  sleep(1)
  processes_with_cmd_in_command = list(filter(lambda ps : cmd in " ".join(ps.cmdline()),psutil.process_iter()))
  try:
    print("Started Process [{}] (PID:{})".format(cmd,processes_with_cmd_in_command[0].pid))
  except IndexError as e:
    print("It seems like no Process with the given command [{}] is running.".format(cmd))
    print(e)
    return None
  return processes_with_cmd_in_command[0]

def remove_duplicates(list : list) -> list:
  """
  Removes all duplicates from the given list.

  :param list: List to remove duplicates from.
  :type list: list
  :return: New list without the duplications of values.
  :rtype: list
  """
  res = [dict(t) for t in {tuple(d.items()) for d in list}]
  return res

def dir_contains_neem(dir : str) -> bool:
  """
  Returns true if the given folder contains a NEEM

  :param dir: Path to check the presence of a NEEM in.
  :type dir: str
  :return: True if the given folder contains a NEEM. Dalse otherwise.
  :rtype: bool
  """
  target_dirs = ["annotations", "tf", "triples"]
  if dir and os.path.isdir(dir):
    return all(os.path.exists(os.path.join(dir,target)) for target in target_dirs)
  else:
     return False

def busy_wait(condition: Callable, max_wait : float = 10, poll_freq : float = 0.1, title : str = "") -> bool:
  """
  Busy waits on a condition to become true.

  :param condition: Condition to become true.
  :type condition: Callable
  :param max_wait: Maximum wait time until busy waiting is exited and False is returned.
  :type max_wait: float
  :param poll_freq: Frequency in which to poll.
  :type poll_freq: float
  :param title: Used in the print output to recognize the waiting condition.
  :type title: string
  """
  waited = 0
  print("Waiting for {}...".format(title))
  while not condition():
    sleep(poll_freq)
    waited += poll_freq
    if waited >= max_wait:
      print("Waited for {} seconds. {} did not start in that time.".format(max_wait,title))
      return False
  return True

def is_triple_count_stable() -> bool:
  """
  Checks whether the count of tribles in the db 'roslog' is stable.

  :return: True if it is stable. False otherwise.
  :rtype: bool
  """
  config = load_config()
  mongo_client = MongoClient("mongodb://{}".format(config.get("mongodb","")))
  if ("roslog" not in mongo_client.list_database_names()) or ("triples" not in mongo_client.get_database("roslog").list_collection_names()):
      print("No \"triples\" collection in database or no \"roslog\" database. Considering triples *unstable*.")
      return False
  query_triple_count = lambda : mongo_client.get_database("roslog").get_collection("triples").count_documents({})
  a = query_triple_count()
  sleep(0.2)
  b = query_triple_count()
  return a == b


def get_lan_ip() -> str:
  """
  Returns the ip address of the first adapter of the system with the format 192.*.*.*.
  
  :return: IP adress of the first adapter of the format 192.*.*.*.
  :rtype: str
  """
  ip_list = list(filter(lambda ip: ip.startswith('192'),check_output(["hostname", "-I"]).decode().split()))
  if not ip_list:
      print("No IP address 192.*.*.* found. Falling back to localhost mode.")
      return "127.0.0.1"
  if not len(ip_list) == 1:
      print("System has more than 1 IP-Address 192.*.*.* . Taking {}".format(ip_list[0]))
  return ip_list[0]

def create_db_dump() -> bool:
  """
  Creates a dump of the current state of the MongoDB and stores it s.t. it is loaded in in the pre-recording procedure directly after dropping the db.
  :return: True when the dump was created successfully. False otherwise.
  :rtype: bool
  """
  dump_path = config.get("db_dump_loc_to_load_as_bootstrap_for_new_neem","")
  if dump_path:    
    Path(dump_path).mkdir(parents=True, exist_ok=True)
  else:
    print("No path to dump the database to is specified in the config.")
    return False
  mongo_client = MongoClient("mongodb://{}".format(config.get("mongodb","")))
  if "roslog" in mongo_client.list_database_names():
    db = mongo_client["roslog"]
    for coll in db.list_collection_names():
      with open(os.path.join(config.get("db_dump_loc_to_load_as_bootstrap_for_new_neem"), f"{coll}.bson"), 'wb+') as f:
        for doc in db[coll].find():
          f.write(bson.BSON.encode(doc))
    return True
  else:
    print("No db 'roslog' in MongoDB. Aborting dump.")
    return False

def delete_db_dump() -> bool:
  """
  Deletes the MongoDB dump that is used as a bootstrapping state, if it exists.
  :return: True if the dump was deleted. False if none existed.
  :rtype: bool
  """
  dump_path = config.get("db_dump_loc_to_load_as_bootstrap_for_new_neem","")
  if dump_path and Path(dump_path).is_dir() and os.listdir(dump_path):
    for file in os.listdir(dump_path):
      os.remove(os.path.join(dump_path,file))
    return True
  else:
    return False