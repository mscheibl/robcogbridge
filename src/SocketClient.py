import zmq
import json

class SocketClient:
    """
    Minimal implementation of a client to locally try out the server functionalities.
    """
    def __init__(self) -> None:
        self.context = zmq.Context()
        self.socket = self.context.socket(zmq.REQ)
    
    def run(self) -> None:
        """
        Start the client, connects to a port on localhost and then queries the user for function calls.

        :return: None
        :rtype: None
        """
        port = input("Enter Port: ")
        print("Connecting to Server >>>",flush=True)
        self.socket.connect("tcp://localhost:{}".format(port))
        self.socket.send_json({"method_name":"ping","method_args":[],"method_kwargs":{}},flags=zmq.NOBLOCK)
        print("Ping...",end="",flush=True)
        if self.socket.poll(3000) == 0:
            print("Could not connect. Is the server up on port: {}?".format(port))
            exit()
        else:
            print(self.socket.recv_json())
            print("Connection established!")
            print("You can now ask the server what methods you can call by calling 'inspect'")

        while True:
            message = {} 
            message["method_name"] = input("Func Name:")
            args = input("Args:")
            kwargs = input("Kwargs:")
            try:
                message["method_args"] = json.loads(args) if args else [] 
                message["method_kwargs"] = json.loads(kwargs) if kwargs else {}
            except json.decoder.JSONDecodeError:
                print("The args/kwargs are not well-formed JSON")
                continue
            self.socket.send_json(message)
            # Response
            res = self.socket.recv_json() 
            print(json.dumps(res,indent=2))

if __name__ == "__main__":
    client = SocketClient()
    client.run()