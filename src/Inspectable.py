import inspect
from typing import List, Dict

class Inspectable:

    """
    Inherit from this class to mark a class as callable over the socket connection.
    The core functionality of this class is the inspect-function which the frontend assumes to exist.
    The frontend uses the inspect method to generate buttons for the available functions of an object that is an Inspectable.
    """

    def __init__(self) -> None:
        """
        Creates a new Inspectable instance. Prints out the ID of the new instance.
        """
        print("{} - ID: {}".format(self.__class__.__name__,id(self)))

    def _class_to_keyword(type_class: type) -> str:
        """
        Return the keyword to identify a type by for the given class.

        :param type_class: Type of class to return the keyword for
        :type type_class: type
        :return: Identifier for the given type.
        :rtype: str
        """
        mapping = {int : "int", float : "float", bool : "bool", str : "string"}
        res = None
        try:
            res = mapping[type_class]
        except KeyError as e:
            print(str(e) + "\nYou called a method with a type in the signature that can not be mapped properly.")
        return res
    
    def inspect(self) -> List[Dict]:
        """
        Returns all methods, signatures and docstrings of an object.

        :return: A list of all functions that an objects exposes (not starting with '_') with its docstring, arguments and argument types (if type hints are given).
        :rtype: list[dict]
        """
        methods = []
        for name, method in inspect.getmembers(self, predicate=inspect.ismethod):
            if not name.startswith("_") :
                try:
                    m = {
                        "method_name" : name,
                        "method_docstring" : inspect.getdoc(method),
                        "method_kwargs" : {},
                        "kwargs_type" : {}}
                    for k,v in inspect.signature(method).parameters.items():
                        m["method_kwargs"][k] = v.default if v.default is not inspect.Parameter.empty else None
                        m["kwargs_type"][k] = Inspectable._class_to_keyword(v.annotation)
                    methods.append(m)
                except ValueError as e:
                    print(str(e) + "\nSkipping Method: " + name)
        return methods