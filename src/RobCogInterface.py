from Inspectable import Inspectable
from KRStartup import KRStartup_Ctrl

class RobCogInterface (Inspectable):
    """
    Wraps an instance of a KRStartup_Ctrl and KRReasoner_Ctrl and bundles the necessary calls between NEEM recordings in two functions.
    This exposes a really simplified set of functions when used as an Inspectable.
    """
    def __init__(self) -> None:
        self.startup_ctrl = KRStartup_Ctrl()
        self.startup_ctrl.start_roscore()
        from KRReasoner import KRReasoner_Ctrl
        self.reasoner_ctrl = KRReasoner_Ctrl(self.startup_ctrl)
        super().__init__()

    def preRecording(self) -> bool:
        """
        Needs to be called before recording the first NEEM and in between recordings.

        :return: True unconditionally.
        :rtype: bool
        """
        if not self.reasoner_ctrl.restore_roslog_from_dump():
            self.reasoner_ctrl.drop_roslog_db()
        self.startup_ctrl.restart_knowrob()
        self.reasoner_ctrl.load_owl()
        self.startup_ctrl.start_rest_server()
        self.startup_ctrl.restart_rviz()
        return True

    def postRecording(self, dir:str = "") -> bool:
        """
        Needs to be called after each NEEM recording but prior to preRecording in between records.
        
        :param dir: Directory to store the recorded NEEM in.
        :type dir: str
        :return: True unconditionally.
        :rtype: bool
        """
        self.reasoner_ctrl.memorize_neem(dir)
        return True
    
    def reviewNEEM(self) -> bool:
        """
        Republishes the NEEM to RVIZ that is currently stored in the MongoDB.
        
        :return: True if the publishing was successful. False otherwise.
        :rtype: bool
        """
        return self.reasoner_ctrl.republish_to_rviz()
    
    def createDBDump(self) -> bool:
        """
        Creates a dump of the current state of the MongoDB and stores it s.t. it is loaded in in the pre-recording procedure directly after dropping the db.
        :return: True when the dump was created successfully. False otherwise.
        :rtype: bool
        """
        return self.reasoner_ctrl.create_dump()

    def deleteDBDump(self) -> bool:
        """
        Deletes the MongoDB dump that is used as a bootstrapping state, if it exists.
        :return: True if the dump was deleted. False if none existed.
        :rtype: bool
        """
        return self.reasoner_ctrl.delete_dump()