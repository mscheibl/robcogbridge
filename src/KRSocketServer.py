import zmq
import signal
import threading
import json
from time import sleep
from Inspectable import Inspectable
from typing import List,Union
from KRUtils import get_lan_ip

class KRSocketServer(Inspectable):

    def __init__(self,ctrl_instance: Union[Inspectable,List[Inspectable]],port: int,localhost: bool= False) -> None:
        """
        Creates a new KnowRob Socket Server.

        :param ctrl_instance: The calls to this server are expected to be interpretable as a function call on this object/ on of the given object. By design, these instance need to be Inspectables.
        :type ctrl_instance: Inspectable | List[Inspectable]
        :param port: On which port this server will be listening.
        :type port: int
        :param localhost: Whether to run the server on the network adapter 192.*.*.* or on localhost (127.0.0.1).
        :type localhost: bool
        """
        if not hasattr(ctrl_instance, "__iter__"):
            ctrl_instance = list(ctrl_instance)
        self.ctrl_instance = ctrl_instance
        self.ctrl_instance.append(self)
        self.cancelled = False
        self.port = port
        self.ip = "*" if localhost else get_lan_ip()
        self.address = "tcp://{}:{}".format(self.ip,self.port)

        def exit_after_time(time):
            sleep(time)
            print("Shutdown took too long. Shutting down ungracefully.", flush=True)
            exit(-1)

        def signal_handler(sig,frame):
            print("Shutting down the server...",flush=True)
            self.cancelled = True
            threading.Thread(target=exit_after_time, args=(6,)).start()
            
        signal.signal(signal.SIGINT, signal_handler)

    def _run(self):
        """
        Starts the server and lets it listen on its socket for incoming messages that are expected to be in JSON format.
        """
        context = zmq.Context()
        socket = context.socket(zmq.REP)
        socket.bind(self.address)
        print("Request server is up on address: {}".format(self.address))
        while not self.cancelled:
            try:
                message = socket.recv(zmq.NOBLOCK)
            except zmq.error.Again:
                sleep(0.1)
            else:
                try:
                    method_call = json.loads(message)
                except json.decoder.JSONDecodeError:
                    response["error"] = "\n".join(["json.decoder.JSONDecodeError","Message could not be deserialized"])
                    print(response["error"])
                    print("Received:")
                    print(f"Content: {str(message)}")
                    print(f"Length: {len(message)}")
                    print("---")
                    socket.send_json(response)
                    continue
                method_name = method_call["method_name"]
                method_kwargs = method_call["method_kwargs"]
                print("Received request: {}".format(method_call))
                call_executed = False
                response = {}
                errors = []
                for instance in self.ctrl_instance:
                    try:
                        func = getattr(instance,method_name)
                        res = func(**method_kwargs)
                        response[str(instance.__class__.__name__)] = res
                        call_executed = True
                        print("Called on {} (ID: {})".format(instance.__class__.__name__,id(instance)))
                    except (AttributeError,ValueError,TypeError) as e:
                        errors.append("Called on {} (ID: {})".format(instance.__class__.__name__,id(instance)))
                        errors.append(str(e))
                        continue
                if not call_executed:
                    error_string= []
                    error_string.append("Error on Server: {}".format(self.address))
                    error_string.append("The method {} could not be called on instances:".format(method_name))
                    for instance in self.ctrl_instance:
                        error_string.append("{} (ID: {})".format(instance.__class__.__name__,id(instance)))
                    error_string.append(">>> Error Trace <<<")
                    for error_msg in errors:
                        error_string.append(error_msg)
                    error_string.append(">>>>>>>>>><<<<<<<<<<")
                    response["error"] = "\n".join(error_string)
                    print(response["error"])
                socket.send_json(response)
        exit(0)
            
    def cancel(self) -> bool:
        """
        Stops the server.

        :return: True unconditionally.
        :rtype: bool
        """
        self.cancelled = True
        return True

    def ping(self) -> str:
        """
        Pongs back a ping.

        :return: Pong!
        :rtype: str
        """
        return "Pong!"
    