import argparse
from KRSocketServer import KRSocketServer
from KRStartup import KRStartup_Ctrl
import KRUtils
from RobCogInterface import RobCogInterface

if __name__ == "__main__":
    parser = argparse.ArgumentParser(prog="Main",
                                     description="RobCogBridge Startup file. Use -h to view options.")
    parser.add_argument('-l','--localhost',action='store_true', help="If set, the server will start on the localhost address instead of picking the 192.*.*.* adapter.")
    parser.add_argument('-p','--port', type=int, default=5555, help="Port that the server listens on.")
    parser.add_argument('-s','--simple', action='store_true', help="If set, the server will only expose a very simple set of functions and does not expose the granular steps.")
    parser.add_argument('-d','--dump',action='store_true', help="Does not start the server. Creates a dump of the current state of the MongoDB to speed up the loading time for recording a NEEM. The dump is used as a bootstrap for the MongoDB prior to loading any other triples into the database.")
    args = parser.parse_args()
    if args.dump:
        if KRUtils.create_db_dump():
            print("Dump created")
        exit(0)
    controller = []
    if args.simple:
        controller.append(RobCogInterface())
    else:
        startup_ctrl = KRStartup_Ctrl()
        controller.append(startup_ctrl)
        startup_ctrl.start_roscore()
        from KRReasoner import KRReasoner_Ctrl
        controller.append(KRReasoner_Ctrl(startup_ctrl))
    KRSocketServer(controller,args.port,args.localhost)._run()
