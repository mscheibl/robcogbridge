from libraries.NEEM_Evaluator.src.neem_evaluator.events import *
from libraries.NEEM_Evaluator.src.neem_evaluator.neem import Neem
from libraries.NEEM_Evaluator.src.neem_evaluator import knowrob
from KRStartup import KRStartup_Ctrl
from KRUtils import *
from Inspectable import Inspectable
import os
import os.path
from os import getcwd,path
from typing import List, Callable, AnyStr
from pathlib import Path
from bson import json_util
import tkinter as tk
from tkinter import messagebox
from tkinter.filedialog import askdirectory
from datetime import datetime
from pymongo import MongoClient

class KRReasoner_Ctrl(Inspectable):
    """
    Controller for the KRReasoner_View.
    Inspectable to be callable from the RobCog frontend.
    """
    def __init__(self,startup_ctrl) -> None:
        """
        Creates a new KnowRob Reasoner Controller (KRReasoner_Ctrl).
        """
        self.startup_ctrl : KRStartup_Ctrl = startup_ctrl
        self.created_files : list[str] = []
        self.loaded_neem : Neem = None
        self.config : dict = load_config()
        super().__init__()


    def crawl_for_owls(self) -> List:
        """
        Returns a list of all OWL files. The search is started from the given directory.

        :return: A list of the absolute paths to all found OWL files.
        :rtype: list
        """
        res = []
        for root,dirs,files in os.walk(os.path.expanduser("~")):
            files = [f for f in files if not f[0] == '.']
            dirs[:] = [d for d in dirs if not d[0] == '.']
            res.extend([path.join(root,f) for f in filter(lambda f : f.endswith(".owl"),files)])
        return res

    
    def load_owl(self, path : str = "") -> bool:
        """
        Loads the given OWL-file in KnowRob.
        When no file name is given, the default OWL file will be loaded in.
        """
        paths = []
        if path:
            paths.extend(path)
        paths.extend(self.config.get("additional_owl",{}))
        success = False
        failed_loads = 0
        if paths:
            print("Loading in following OWLs: {}".format(paths))
            for p in paths:
                if os.path.isfile(p):
                    knowrob.load_owl(p)
                    print("Loading in OWL file: {}".format(p))
                    busy_wait(is_triple_count_stable,poll_freq=0.4,title="Stable Triple Count on OWL Loading")
                    success = True
                else:
                    failed_loads += 1
                    print("{} does not exist.".format(p))
            if failed_loads:
                print("Failed Additional OWL Loads: {}".format(failed_loads))
        return success
    
    def memorize_neem(self,dir : str = "") -> bool:
        """
        Lets KnowRob save the NEEM that is currently hold in the MongoDB. The root folder for the dir that is given as a parameter is always "/home/user".

        :param dir: Output path for NEEM folder. If left blank, a new directory with a date and time tag will be placed in the users root directory.
        :type dir: str
        :return: True unconditionally.
        :rtype: bool
        """
        busy_wait(self.saveguard_neem_record_finished,max_wait = 30, poll_freq=2,title="Episode Record Finished.")
        if not dir:
            dt = datetime.now()
            dir = "{}-{}-{}_{}-{}-{}".format(dt.year,dt.month,dt.day,dt.hour,dt.minute,dt.second)
        knowrob.memorize_neem(os.path.join(os.path.expanduser("~"), dir))
        return True

    def drop_roslog_db(self) -> bool:
        """
        Drops the roslog databaase from the MongoDB.

        :return: True if the roslog DB exists. False otherwise.
        :rtype: bool
        """
        mongo_client = MongoClient("mongodb://{}".format(self.config.get("mongodb","")))
        if "roslog" in mongo_client.list_database_names():
            mongo_client.drop_database("roslog")
            return True
        else:
            return False
        
    def restore_roslog_from_dump(self) -> bool:
        """
        Restores the state of the 'roslog' database based on the dump that was created by callig Main.py with the -d flag.

        :return: True if the restore was successful. False, if no dump was found.
        :rtype: bool
        """
        self.drop_roslog_db()
        dump_path = self.config.get("db_dump_loc_to_load_as_bootstrap_for_new_neem")
        if dump_path and os.path.isdir(dump_path) and list(filter(lambda file: file.endswith('.bson'), os.listdir(dump_path))):
            mongo_client = MongoClient("mongodb://{}".format(self.config.get("mongodb","")))
            db = mongo_client["roslog"]
            for coll in filter(lambda file: file.endswith('.bson'), os.listdir(dump_path)):
                    with open(os.path.join(dump_path, coll), 'rb+') as f:
                        content = bson.decode_all(f.read())
                        if content:
                            db[coll.split('.')[0]].insert_many(content)
            return True
        else:
            return False
        
    def create_dump(self) -> bool:
        """
        Creates a dump of the current state of the MongoDB and stores it s.t. it is loaded in in the pre-recording procedure directly after dropping the db.
        :return: True when the dump was created successfully. False otherwise.
        :rtype: bool
        """
        return create_db_dump()
    
    def delete_dump(self) -> bool:
        """
        Deletes the MongoDB dump that is used as a bootstrapping state, if it exists.
        :return: True if the dump was deleted. False if none existed.
        :rtype: bool
        """
        return delete_db_dump()
    
    @init_prolog   
    def saveguard_neem_record_finished(self) -> bool:
        query = knowrob.prolog.once("""
            is_episode(Epi), is_setting_for(Epi,Act), is_action(Act), 
            has_time_interval(Act,T), has_interval_begin(T,Start), has_interval_end(T, End)""")
        if query:
            print(query)
            return True 
        else:
            print('.',end="")
            return False
    
    @init_prolog
    def republish_to_rviz(self) -> bool:
        """
        Republishes the currently loaded NEEM to RVIZ.
        
        :return: True if the tf data is successfully loaded into RVIZ. False otherwise.
        :rtype: bool
        """
        busy_wait(self.saveguard_neem_record_finished,max_wait = 30, poll_freq=2,title="Episode Record Finished.")
        query = knowrob.prolog.once("is_episode(Epi), is_setting_for(Epi,Act), is_action(Act)")
        if query:
            main_action = query["Act"]
            print("MAIN ACTION: {}".format(main_action))
            query = knowrob.prolog.once("has_time_interval('{}',T), has_interval_begin(T,Start), has_interval_end(T, End)".format(main_action))
            if query:
                start = query["Start"]
                end = query["End"]
                print("START: {}".format(start))
                print("END: {}".format(end))
                self.startup_ctrl.restart_rviz()
                knowrob.prolog.once("tf_plugin:tf_republish_set_goal({}, {})".format(start,end))
                return True
        return False
    
    def remember_neem(self,dir : str = "") -> bool:
        """
        Remembers the NEEM in the given path.

        :param dir: (Absolute) Path to the NEEM
        :type dir: str
        :return: True if the NEEM was found in the given path and was successfully loaded by KnowRob. False otherwise.
        :rtype: bool
        """
        if dir_contains_neem(dir):     
            knowrob.remember_neem(dir)
            self.loaded_neem = Neem()
            return True
        else:
            return False

    def crawl_for_neems(self)-> List:
        """
        Returns a list of all dirctories that include a NEEM (starting search from home-directory).

        :return: A list of the absolute paths to all directories that include a NEEM.
        :rtype: list
        """
        res = []
        for root,dirs,_ in os.walk(os.path.expanduser("~")):
            dirs[:] = [d for d in dirs if not d[0] == '.']
            if dir_contains_neem(root):
                res.append(root)
        return res

    def get_all_actions_in_neem(self) -> List:
        """
        Returns all Actions in the loaded NEEM.

        :return: A list of all Actions in the currently loaded NEEM.
        :rtype: list
        """
        res = [act.to_json() for act in self.loaded_neem.get_all_actions_in_neem()]
        for action in res:
            action["TIME LENGTH"] = action["end"] - action["start"]
            action["OBJECT COUNT"] = len(action["objects"])
            if not action["participants"]:
                del action["participants"]
            del action["objects"]
        return res

    def get_all_objects_in_neem(self) -> List:
        """
        Returns all Objects in the loaded NEEM.

        :return: A list of all Objects in the currently loaded NEEM.
        :rtype: list
        """
        res = [[{k:o.to_json()[k] for k in ["name","instance"]} for o in act.objects] for act in self.loaded_neem.get_all_actions_in_neem()]
        res = sorted(remove_duplicates([item for sublist in res for item in sublist]),key=lambda d : d["name"])
        return res
    
    def get_link_name_for_object(self) -> List[Dict]:
        """
        Returns all Link Names for all Objects in the loaded NEEM.

        :return: A list of dictionaries where each dictionary includes the name, instances and link name for each Object in the currently loaded NEEM.
        :rtype: list[dict]
        """
        objects = self.loaded_neem.get_all_objects_in_neem()
        return [{k:obj.to_json()[k] for k in ["name","instance","link_name"]} for obj in objects]

class KRReasoner_View:
    """
    If you want to use the KRReasoner_Ctrl without the frontend within RobCog,
    you can use this class to get a simple GUI to interface with the the functions of KRReasoner_Ctrl.
    """

    TMP_FOLDER = "tmp"

    class SelectionWindow():
        """
        A window that lets the user select between different options offered by buttons.
        """
        def __init__(self, entities_to_create_button_for : List, on_click : Callable, title : str) -> None:
            """
            Returns a list of all dirctories that include a NEEM. The search is started from the given directory.

            :param entities_to_create_button_for: List of Dicts to create bottons for. Which information from the dict is used to name the button is defined by name_func.
            :type entities_to_create_button_for: list
            :param on_click: Function to call when a button is pressed
            :type on_click: Callable
            :param title: Title of the Selection Window
            :type title: str
            """
            self.action_selector = tk.Tk()
            self.action_selector.title(title)
            self.on_click = on_click
            self.action_selector.protocol("WM_DELETE_WINDOW", lambda : self.action_selector.destroy)
            naming_func = lambda e : "{}_{}".format(e.name,e.instance[e.instance.find("#")+1:len(e.instance)])
            for e in entities_to_create_button_for:
                btn = tk.Button(self.action_selector, text=naming_func(e), width=BTN_WIDTH, command= lambda e = e : self.on_click(e))
                btn.pack(padx=PADX,pady=PADY)

    def gui_remember_neem(self) -> None:
        """
        Wrapper for remember_neem that opens up a file explorer.
        """
        dir = askdirectory()
        if self.ctrl.remember_neem(dir):
            self.neem_label.config(text = "Selected NEEM: " + dir)
            for btn in self.reasoner_btns:
                btn["state"] = "normal"
        else:
            tk.messagebox.showinfo("Warning", "The selected folder does not include the typical NEEM structure\n(\"annotations\"-, \"triples\"-, \"tf\"-folder)")
            
    def output_to_fileviewer(self,content,filename: str) -> str:
        """
        Opens up a cmd text editor (the default one).

        :param content: content to display
        :type content: list/dict
        :param filename: name of the temporary file to create
        :type filename: str
        :return: Path to the temporary text file that is opened up in the editor
        :rtype: str
        """
        Path(os.path.join(getcwd(),KRReasoner_View.TMP_FOLDER)).mkdir(parents=True, exist_ok=True)
        out = os.path.join(getcwd(),KRReasoner_View.TMP_FOLDER,("{}_OUT.txt".format(filename)))
        with open(out,'w') as f:
            f.write(json_util.dumps(content, indent=2))
            self.ctrl.created_files.append(out)
        run_cmd_in_gnome_term("/usr/bin/editor {}".format(out))
        return out

    def wrapper_get_objects_for_action(self) -> None:
        """
        GUI-Selector for Action that outputs all Objects that occur within the selected Action.
        """
        def on_click(action):
            res = action.to_json()["objects"]
            for obj in res:
                del obj["tf_list"]
            self.output_to_fileviewer(res, "objects_for_action_{}".format(action.name[action.name.find("#")+1:len(action.name)]))
        KRReasoner_View.SelectionWindow(self.ctrl.loaded_neem.get_all_actions_in_neem(),on_click,"Get all Objects an for Action",self)

    def wrapper_get_actions_for_object(self) -> None:
        """
        GUI-Selector for Object that outputs all Actions in which the Object occurs.
        """
        def on_click(obj):
            res = [act.to_json() for act in self.ctrl.loaded_neem.get_all_actions_in_neem() if obj in act.get_all_objects_for_action()]
            cleaned_res = []
            for act in res:
                cleaned_act = {k:act[k] for k in ["name","instance"]}
                cleaned_act["objects"] = [{l:o[l] for l in ["name","instance"]} for o in act["objects"]]
                cleaned_res.append(cleaned_act)
            self.output_to_fileviewer(cleaned_res,"actions_for_object_{}".format(obj.name))
        KRReasoner_View.SelectionWindow(self.ctrl.loaded_neem.get_all_objects_in_neem(),on_click,"Get All Actions for an Object",self)

    def wrapper_get_all_tf_for_action(self) -> None:
        """
        Returns the TF data for some action.
        """
        def on_click(action):
            res = [obj.to_json() for obj in action.objects]
            self.output_to_fileviewer(res,"tf_for_action_{}".format(action.name[action.name.find("#")+1:len(action.name)]))
        KRReasoner_View.SelectionWindow(self.ctrl.loaded_neem.get_all_actions_in_neem(),on_click,"Get TF for Action",self)

    def _on_closing(self) -> None:
        """
        Called when the X-button is pressed in the top-right corner.
        """
        if os.path.exists(os.path.join(getcwd(),KRReasoner_View.TMP_FOLDER)) and len(os.listdir(os.path.join(getcwd(),KRReasoner_View.TMP_FOLDER))) > 0:
            if not messagebox.askyesno(title="Temporary Files Cleanup", message="Do you want to keep the query result files?"):
                for path in self.ctrl.created_files:
                    if os.path.exists(path):
                        os.remove(path)
        self.root.destroy()

    def _add_button(self,text: str,on_click: Callable) -> tk.Button:
        """
        Configuring and adding a button to the main GUI of the reasoner interface.

        :param text: Text of the button
        :type text: str
        :param on_click: Command of the button to trigger
        :type on_click: Callable
        """
        btn = tk.Button(self.root, text = text, width = BTN_WIDTH, command = on_click)
        btn.pack(pady=PADY,padx=PADX)
        self.reasoner_btns.append(btn)
        return btn

    def _add_space(self) -> None:
        """
        Adds a space into the main GUI of the reasoner interface.
        """
        space = tk.Label(self.root, text="")
        space.pack(pady=PADY,padx=PADX)

    def __init__(self):
        """
        Configurates the main interface of the reasoner GUI.
        """
        self.reasoner_btns = []   
        self.ctrl = KRReasoner_Ctrl()
        self.root = tk.Tk()
        self.root.title("Reasoner")
        self.root.resizable(False, False)
        self.root.protocol("WM_DELETE_WINDOW", self._on_closing)
        self.root.option_add('*Dialog.msg.font', 'Helvetica 11')

        self.neem_label = tk.Label(self.root,text="No NEEM folder selected")
        self.neem_label.pack(padx=PADX,pady=PADY)
        btn_remember_neem = self._add_button("Remember NEEM", self.gui_remember_neem)
        btn_all_actions = self._add_button("Get All Actions in NEEM", lambda : self.output_to_fileviewer(self.ctrl.get_all_actions_in_neem(),"all_actions_in_neem"))
        btn_all_objs = self._add_button("Get All Objects in NEEM", lambda : self.output_to_fileviewer(self.ctrl.get_all_objects_in_neem(),"all_objects_in_neem"))
        self._add_space()          
        btn_obj_for_action = self._add_button("Get Objects for Action", self.wrapper_get_objects_for_action)
        btn_tf_for_action = self._add_button("Get TF for Action", self.wrapper_get_all_tf_for_action)
        self._add_space()
        btn_action_for_obj = self._add_button("Get Actions for Object", self.wrapper_get_actions_for_object)
        btn_link_for_obj = self._add_button("Get Link Name for Object", lambda : self.output_to_fileviewer(self.get_link_name_for_object(),"link_names_for_objects"))
        self._add_space()
        btn_plot = self._add_button("Plot Events as Bar Chart", lambda : plot_events(self.ctrl.loaded_neem))
        
        btn_all_actions["state"]="disabled"
        btn_all_objs["state"]="disabled"
        btn_obj_for_action["state"]="disabled"
        btn_tf_for_action["state"]="disabled"
        btn_action_for_obj["state"]="disabled"
        btn_link_for_obj["state"]="disabled"
        btn_plot["state"]="disabled"

        self.root.mainloop()

if __name__ == "__main__":
    KRReasoner_View()