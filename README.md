# RobCogBridge

## Getting Started
1. Clone the repository
2. Don't forget to pull the submodules too: `git submodules update --recursive --init --remote` from the project's root folder
3. Create a new virtual Python environment: `python3 -m venv .venv` in the project's root folder
4. Start up your new virtual environment: `source .venv/bin/activate`
5. Install the Python requirements in the virtual environment: `pip install -r requirements.txt`
6. Make sure that everything ROS-related is setup to run _KnowRob_, _RViz_, and the _RESTClient_.
    * If you don't feel like doing that, use the bootstrap-scripts on a VM. Read more [here](https://gitlab.ub.uni-bielefeld.de/mscheibl/knowrob-provisioning).
7. Start up the server: `python3 src/Main.py`
    * Use the -l flag for local testing (the server will start on localhost)
    * Use the -s flag for a simplified set of exposed functions
8. If you want to do some local playing-around start the simple SocketClient: ``python3 src/SocketClient.py`
    * The default port that the server that you started is listening on is **5555**
9. Read the docs: Open `doc/build/html/index.html` in a browser
