import os
import sys
sys.path.insert(0, os.path.abspath(os.path.join("..","..","src")))

project = 'RobCogBridge'
copyright = '2024, Manuel Scheibl'
author = 'Manuel Scheibl'
release = '0.1'

extensions = [
   'sphinx.ext.duration',
   'sphinx.ext.doctest',
   'sphinx.ext.autodoc',
   'sphinx.ext.autosummary',
   'sphinx.ext.coverage'
]

templates_path = ['_templates']
exclude_patterns = []

html_theme = 'alabaster'
